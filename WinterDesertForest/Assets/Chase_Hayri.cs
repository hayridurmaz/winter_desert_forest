﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AI;

public class Chase_Hayri : MonoBehaviour
{
    public Transform player;
    public Animator enemyAnim;
    public Slider healthBar;
    NavMeshAgent nav;
    
    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Arrow")
        {
            healthBar.value -= Random.Range(5, 20);
            if (healthBar.value < 1)
            {
                enemyAnim.SetBool("isDead", true);
                enemyAnim.SetBool("isAttacking", false);
                Destroy(this.gameObject, 2);
            }
            else
            {
                enemyAnim.SetBool("isDead", false);
            }

        }
    }
    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        nav.stoppingDistance = 3f;
        enemyAnim = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player").transform;
    }

    void Update()
    {
        
        if (Vector3.Distance(player.position, this.transform.position) < 20 && enemyAnim.GetBool("isDead") == false)
        {
            Vector3 direction = player.position - this.transform.position;
            direction.y = 0;

            nav.enabled = true;
            nav.SetDestination(player.position);


            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);

            enemyAnim.SetBool("isIdle", false);

            if (direction.magnitude > 3f)
            {
                
                //this.transform.Translate(0, 0, 0.05f);
                enemyAnim.SetBool("isWalking", true);
                enemyAnim.SetBool("isAttacking", false);
            }
            else
            {
                
                enemyAnim.SetBool("isWalking", false);
                enemyAnim.SetBool("isAttacking", true);

            }
        }
        else
        {
            nav.enabled = false;

            enemyAnim.SetBool("isIdle", true);
            enemyAnim.SetBool("isAttacking", false);
            enemyAnim.SetBool("isWalking", false);

        }
    }
}