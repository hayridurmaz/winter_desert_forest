﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireController : MonoBehaviour {

    bool isFire;
    bool isGirdi;
	// Use this for initialization
	void Start () {
        isFire = false;
        isGirdi = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(isGirdi && isFire)
        {
            WinterSceneManager wm = GameObject.FindWithTag("Player").GetComponent<WinterSceneManager>();

            if (WinterSceneManager.heat <wm.heatBar.maxValue)
            {
                WinterSceneManager.heat += Time.deltaTime * 10;
            }
        }
	}

    public void lightFire()
    {
        ParticleSystem[] fireEmitters = gameObject.GetComponentsInChildren<ParticleSystem>();
        for(int i=0; i < fireEmitters.Length; i++)
        {
            ParticleSystem.EmissionModule em = fireEmitters[i].emission;
            em.enabled = true;
        }
        gameObject.GetComponent<AudioSource>().Play();
        isFire = true;
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player" )
        {
            isGirdi = true;
            if(WinterSceneManager.woods >= 10)
            {
                if (!isFire)
                {

                    if (WinterSceneManager.boats < 1)
                    {
                        other.GetComponent<WinterSceneManager>().changeHint("You should always come back here for warming during the game, now go find boots for moving faster!");
                    }
                    else if (WinterSceneManager.gloves < 1)
                    {
                        other.GetComponent<WinterSceneManager>().changeHint("You should always come back here for warming during the game, now go find gloves for killing enemies!");
                    }
                    else if(WinterSceneManager.diamonds<7)
                    {
                        other.GetComponent<WinterSceneManager>().changeHint("You should always come back here for warming during the game, now go find all diamonds");
                    }
                    else
                    {
                        other.GetComponent<WinterSceneManager>().changeHint("You should always come back here for warming during the game, now go to outpost fast!");
                    }
                    
                    
                }
                else
                {
                    gameObject.GetComponent<AudioSource>().Play();
                    if (WinterSceneManager.boats < 1)
                    {
                        other.GetComponent<WinterSceneManager>().changeHint("You should always come back here for warming during the game, now go find boots for moving faster!");
                    }
                    else if (WinterSceneManager.gloves < 1)
                    {
                        other.GetComponent<WinterSceneManager>().changeHint("You should always come back here for warming during the game, now go find gloves for killing enemies!");
                    }
                    else if (WinterSceneManager.diamonds < 7)
                    {
                        other.GetComponent<WinterSceneManager>().changeHint("You should always come back here for warming during the game, now go find all diamonds");
                    }
                    else
                    {
                        other.GetComponent<WinterSceneManager>().changeHint("You should always come back here for warming during the game, now go to outpost fast!");
                    }

                }
                lightFire();
                
            }
            else
            {
                other.GetComponent<WinterSceneManager>().changeHint("You need more woods. Look carefully");
            }
        }

    }



    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            isGirdi = false;
            gameObject.GetComponent<AudioSource>().Pause();
        }
    }



}
