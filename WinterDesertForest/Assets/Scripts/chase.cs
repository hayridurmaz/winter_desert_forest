﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class chase : MonoBehaviour {
    private Transform player;
    public Animator enemyAnim;
    public Slider healthBar;
    
    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.tag == "Arrow")
        {
            healthBar.value -= Random.Range(5, 20);
            if (healthBar.value<1)
            {
                enemyAnim = GetComponent<Animator>();
                enemyAnim.SetBool("isDead", true);
                enemyAnim.SetBool("isAttacking", false);
                Destroy(this.gameObject, 2);
            }
            else
            {
                enemyAnim.SetBool("isDead", false);
            }

        }
        
    }
    void Start () {
        enemyAnim = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player").transform;
       
    }

    void Awake()
    {
        enemyAnim = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player").transform;
    }


	void Update () {

        if (Vector3.Distance(GameObject.Find("FPSController").transform.position, this.transform.position) < 20)

        if (Vector3.Distance(player.position, this.transform.position) < 20 && enemyAnim.GetBool("isDead")==false)

        {
            Vector3 direction = GameObject.Find("FPSController").transform.position - this.transform.position;
            direction.y = 0;

            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);

            enemyAnim.SetBool("isIdle", false);
            
            if (direction.magnitude > 3 )
            {
                this.transform.Translate(0, 0, 5f*Time.deltaTime);
                enemyAnim.SetBool("isWalking", true);
                enemyAnim.SetBool("isAttacking", false);
            }
            else
            {
                enemyAnim.SetBool("isWalking", false);
                enemyAnim.SetBool("isAttacking", true);

            }
        }
        else
        {
            enemyAnim.SetBool("isIdle", true);
            enemyAnim.SetBool("isAttacking", false);
            enemyAnim.SetBool("isWalking", false);
            
        }
	}
}