﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class detectHit : MonoBehaviour {

    public Slider healthBar;

    void OnTriggerEnter(Collider other)
    {
        
        if (other.gameObject.tag == "Player")
        {
            if (healthBar != null)
            {
                if (this.gameObject == GameObject.FindGameObjectWithTag("Demon"))
                {
                    healthBar.value -= Random.Range(20, 50);
                }
                else
                {
                    healthBar.value -= Random.Range(5, 20);
                }

            }
            else if(SceneManager.GetActiveScene().name=="Winter")
            {
                WinterSceneManager sn = GameObject.FindWithTag("Player").GetComponent<WinterSceneManager>();
                sn.healthDecrease(gameObject.name);
            }
            
        }
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
