﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {
    public Texture2D[] meterCharge;
    public Renderer meter;
    public static int charge = 0;
    public AudioClip collectSound;
    public Texture2D[] hudCharge;
    public UnityEngine.UI.RawImage chargeHUDGUI;
	// Use this for initialization
	void Start () {
        charge = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

     void CellPickup()
    {
        AudioSource.PlayClipAtPoint(collectSound, transform.position);
        charge++;
        chargeHUDGUI.texture = hudCharge[charge];
        meter.material.mainTexture = meterCharge[charge];

    }
}
