﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Timers;
using UnityEngine.SceneManagement;

public class WaterAndHealth : MonoBehaviour {
    public Slider WaterBar;
    public Slider healthBar;
    public float waitTime = 1f;
    float timer,deathtimer,time;
    public Text hintText;
    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (timer > waitTime)
        {
            WaterBar.value = WaterBar.value - 1;
            timer = 0f;
        }
       
        if (healthBar.value <= 1 || WaterBar.value <= 1)
        {
            deathtimer += Time.deltaTime;
            hintText.color = Color.red;

            hintText.text = "You died :(";
        }
        if (deathtimer > 3)
        {
            SceneManager.LoadScene("Desert");
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Hint")
        {
            hintText.color = Color.yellow;
            hintText.text = "You can get hints from cat statues!!";
            
        }
        if (other.tag == "WaterPot")
        {
            Destroy(other.gameObject);
            WaterBar.value += 10;
        }
       
        if (other.tag == "LifePot")
        {
            Destroy(other.gameObject);
            healthBar.value += 10;
        }
        if(other.tag == "Fire1"||other.tag=="Fire5")
        {
            healthBar.value -= 25;
        }
        
    }

}
