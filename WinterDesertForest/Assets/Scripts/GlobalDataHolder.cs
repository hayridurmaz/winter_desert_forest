﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class GlobalDataHolder
{


    private static Dictionary<string, object> routeParams = new Dictionary<string, object>();

    public static void PutRouteParams(string key, object value)
    {
        //routeParams[key] = value;
        string dataPath = Path.Combine(Application.persistentDataPath, "LOAD.txt");
        

        using (StreamWriter streamWriter = File.CreateText(dataPath))
        {
            streamWriter.Write(key+":"+value);
        }
    }

    public static object GetRouteParams(string key)
    {
        using (StreamReader streamReader = File.OpenText(Path.Combine(Application.persistentDataPath, "LOAD.txt")))
        {
            string jsonString = streamReader.ReadLine();
            if (jsonString == null)
            {
                return "NODATA";
            }
            if (jsonString.Contains(key+":"))
            {
                return jsonString.Substring(jsonString.IndexOf(":")+1);
            }
            else
            {
                return "NODATA";
            }
        }

       // bool isKeyFound = routeParams.TryGetValue(key, out value);
       // return isKeyFound ? value : isKeyFound;
    }

    public static bool RemoveRouteParams(string key)
    {
        return routeParams.Remove(key);
    }

    public static bool isKeyExist(string key)
    {
        return routeParams.ContainsKey(key);
    }

    public static void ClearGlobalDataHolder()
    {
        routeParams.Clear();
    }
}
