﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForestDoor : MonoBehaviour
{


    static int cikisSayisi = 0;
    public static bool KapiAcilsinMi = true;
    public Light doorLight;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void KapidanGecti()
    {
        if (cikisSayisi == 0)
        {
            doorLight.color = Color.red;
            cikisSayisi++;
            KapiAcilsinMi = false;
        }
    }
}
