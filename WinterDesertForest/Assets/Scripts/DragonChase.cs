﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragonChase : MonoBehaviour
{
    public Transform player;
    public Animator enemyAnim;
    public int health = 100;
    //public Slider healthBar;

    private void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Arrow")
        {
            health -= Random.Range(5, 20);
            // healthBar.value -= Random.Range(5, 20);
            if (health < 1)
            {
                enemyAnim.SetBool("isDead", true);
                enemyAnim.SetBool("isAttacking", false);
                Destroy(this.gameObject, 2);
            }
            else
            {
                enemyAnim.SetBool("isDead", false);
            }

        }
    }
    void Start()
    {
        enemyAnim = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player").transform;
    }

    void Update()
    {
        Debug.Log(health);
        if (Vector3.Distance(player.position, this.transform.position) < 20)
        {
            Vector3 direction = player.position - this.transform.position;
            direction.y = 0;

            this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.LookRotation(direction), 0.1f);

            enemyAnim.SetBool("isIdle", false);
            if (direction.magnitude > 5)
            {
                this.transform.Translate(0, 0, 6f*Time.deltaTime);
                enemyAnim.SetBool("isWalking", true);
                enemyAnim.SetBool("isAttacking", false);
            }
            else
            {
                enemyAnim.SetBool("isWalking", false);
                enemyAnim.SetBool("isAttacking", true);

            }
        }
        else
        {
            enemyAnim.SetBool("isIdle", true);
            enemyAnim.SetBool("isAttacking", false);
            enemyAnim.SetBool("isWalking", false);

        }
    }
}