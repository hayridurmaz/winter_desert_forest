﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerOutpost : MonoBehaviour {

    public Light doorLight;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "FPSController")
        {

            if (WinterSceneManager.KapiAcilsinMi)
            {
                transform.FindChild("door").SendMessage("DoorCheck");
            }
             
            
           
        }

    }
}
