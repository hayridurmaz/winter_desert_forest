﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fire : MonoBehaviour {
    Renderer rend,rend2,rend3,rend4,rend5,rend6;
    BoxCollider box,box2,box3,box4,box5;
    float time;
    public Text hintText;
    public Slider healthBar;
    // Use this for initialization
    void Start () {
        rend = GameObject.FindGameObjectWithTag("Fire1").GetComponent<Renderer>();
        box = GameObject.FindGameObjectWithTag("Fire1").GetComponent<BoxCollider>();
        box.enabled = false;
        rend.enabled = false;
        rend2 = GameObject.FindGameObjectWithTag("Fire2").GetComponent<Renderer>();
        rend2.enabled = false;
        rend3 = GameObject.FindGameObjectWithTag("Fire3").GetComponent<Renderer>();
        rend3.enabled = false;
        rend4 = GameObject.FindGameObjectWithTag("Fire4").GetComponent<Renderer>();
        rend4.enabled = false;
        rend5 = GameObject.FindGameObjectWithTag("Fire5").GetComponent<Renderer>();
        box5 = GameObject.FindGameObjectWithTag("Fire5").GetComponent<BoxCollider>();
        box5.enabled = true;
        rend5.enabled = true;
        rend6 = GameObject.FindGameObjectWithTag("Fire6").GetComponent<Renderer>();
        rend6.enabled = true;
    }
    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            rend.enabled = true;
            box.enabled = true;
            rend2.enabled = true;
            rend3.enabled = true;
            rend4.enabled = true;
        
            hintText.text = "WATCH YOUR BACK!!";
            if (healthBar.value < 1||healthBar==null)
            {
                box5.enabled = false;
                rend5.enabled = false;
                rend6.enabled = false;
            }

        }
        
        
    }
    }
