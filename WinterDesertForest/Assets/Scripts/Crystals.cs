﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Crystals : MonoBehaviour
{
    public int crystalNumber;
    public Text Stone;
    public GameObject fire;
    public Text hintText;

    float timer = 0;
    bool timerActive = false;
    // Use this for initialization
    void Start()
    {
        crystalNumber = 0;

    }

    void Update()
    {
        if (timerActive)
        {
            timer += Time.deltaTime;

            if (timer > 3)
            {
                LoadingManager.willBeLoaded = "WinningScene";
                SceneManager.LoadScene("Loading");
            }
        }
    }
    
    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "GreenStone")
        {
            Destroy(other.gameObject);
            crystalNumber++;
        }
        if (other.tag == "BlueStone")
        {
            Destroy(other.gameObject);
            hintText.color = Color.blue;
            hintText.text = "YOU WON!!!!!";
            timerActive =true;
            timer = 0;
        }
        Stone.text = crystalNumber.ToString();
        
        if (other.tag == "DesertDoor")
        {
            if (crystalNumber == 3)
            {
                Destroy(other.gameObject);
            }
            else
            {
                Debug.Log("You can't enter");
            }

        
        }
        if (other.tag == "DesertDoor2")
        {
            if (crystalNumber >= 7)
            {
                Destroy(other.gameObject);
            }
            else 
            {
                Debug.Log("You can't enter");
            }


        }
    }
    
}
