﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityStandardAssets.Characters.FirstPerson;
using GreatArcStudios;

public class WinterSceneManager : MonoBehaviour {


    static int cikisSayisi=0;
    public static bool KapiAcilsinMi= true;
    bool canAttack;
    bool itemsShown = false;
    public Light doorLight;
    public static float health, heat;
    public Slider healthBar, heatBar;
    Text EldivenText, HintText, OdunText, BoatText, DiamondText;
    public static int gloves;
    public static int woods;
    public static int boats;
    public static int diamonds;
    float deathTimer;
    FXDemoController weatherController;
    public AudioClip collectSound;



    // Use this for initialization
    void Start () {
        health = healthBar.maxValue;
        heat = (heatBar).maxValue;
        healthBar.value = healthBar.maxValue;
        heatBar.value = heatBar.maxValue;
        gloves = 0;
        woods = 0;
        boats = 0;
        deathTimer = 0;
        diamonds = 0;
        KapiAcilsinMi = true;
        canAttack = false;

        EldivenText= GameObject.Find("EldivenText").GetComponent<Text>();
        OdunText = GameObject.Find("OdunText").GetComponent<Text>();
        HintText = GameObject.Find("HintText").GetComponent<Text>();
        BoatText = GameObject.Find("BotText").GetComponent<Text>();
        DiamondText = GameObject.Find("DiamondText").GetComponent<Text>();
        BoatText.text = "" + boats;
        HintText.text = "If I were u, I would go left";
        OdunText.text = "" + woods;
        EldivenText.text = "" + gloves;
        DiamondText.text = "" + diamonds;

        weatherController = GameObject.Find("FX").GetComponent<FXDemoController>();
        weatherController.Snow();



        //PauseManager menu = GameObject.Find("Pause Menu Manager").GetComponent<PauseManager>();
        //menu.uiEventSystem.SetSelectedGameObject(menu.defualtSelectedMain);

        //menu.mainPanel.SetActive(false);
        //menu.vidPanel.SetActive(false);
        //menu.audioPanel.SetActive(false);
        //menu.TitleTexts.SetActive(false);
        //menu.mask.SetActive(false);
        //for (int i = 0; i < menu.otherUIElements.Length; i++)
        //{
        //    menu.otherUIElements[i].gameObject.SetActive(true);
        //}




    }

    void awake()
    {
        
    }

    // Update is called once per frame
    void Update () {
        heat -= Time.deltaTime*2f;
        healthBar.value = health;
        heatBar.value = heat;

        //DİE
        if(heat<1 || health < 1)
        {
            deathTimer += Time.deltaTime;
            HintText.color = Color.red;

            HintText.text = "You died :(";
            
        }
        if (deathTimer > 3)
        {
            SceneManager.LoadScene("Winter");
        }


        //Items show, remove
        if (Input.GetKeyDown(KeyCode.I))
        {
            itemsShown = !itemsShown;
        }
        if (!itemsShown)
        {
            GameObject.Find("EldivenText").GetComponent<CanvasGroup>().alpha = 0f;
            GameObject.Find("OdunText").GetComponent<CanvasGroup>().alpha = 0f;
            GameObject.Find("BotText").GetComponent<CanvasGroup>().alpha = 0f;
            GameObject.Find("DiamondText").GetComponent<CanvasGroup>().alpha = 0f;
            GameObject.Find("EldivenImage").GetComponent<CanvasGroup>().alpha = 0f;
            GameObject.Find("OdunImage").GetComponent<CanvasGroup>().alpha = 0f;
            GameObject.Find("BotImage").GetComponent<CanvasGroup>().alpha = 0f;
            GameObject.Find("DiamondImage").GetComponent<CanvasGroup>().alpha = 0f;
            GameObject.Find("InventoryText").GetComponent<CanvasGroup>().alpha = 1f;
        }
        else
        {
            GameObject.Find("EldivenText").GetComponent<CanvasGroup>().alpha = 1f;
            GameObject.Find("OdunText").GetComponent<CanvasGroup>().alpha = 1f;
            GameObject.Find("BotText").GetComponent<CanvasGroup>().alpha =1f;
            GameObject.Find("DiamondText").GetComponent<CanvasGroup>().alpha = 1f;
            GameObject.Find("EldivenImage").GetComponent<CanvasGroup>().alpha = 1f;
            GameObject.Find("OdunImage").GetComponent<CanvasGroup>().alpha = 1f;
            GameObject.Find("BotImage").GetComponent<CanvasGroup>().alpha = 1f;
            GameObject.Find("DiamondImage").GetComponent<CanvasGroup>().alpha = 1f;
            GameObject.Find("InventoryText").GetComponent<CanvasGroup>().alpha = 0f;
        }



       
        
        

    }



 
    

    public void onGetWood()
    {
        woods++;
        if (collectSound != null)
        {
            AudioSource.PlayClipAtPoint(collectSound, transform.position);
        }
        OdunText = GameObject.Find("OdunText").GetComponent<Text>();
        OdunText.text = "" + woods;
        if (woods >= 10)
        {
            changeHint("Go find campfire, set the fire to warm up.");
        }
    }

    public void onGetGlove()
    {
        gloves++;
        if (collectSound != null)
        {
            AudioSource.PlayClipAtPoint(collectSound, transform.position);
        }
        EldivenText = GameObject.Find("EldivenText").GetComponent<Text>();
        EldivenText.text = "" + gloves;
        GameObject.Find("FirstPersonCharacter").GetComponent<Shot_Hayri>().canAttack = true;
        changeHint("You may shot onto the enemies now!");
        canAttack = true;
    }

    public void onGetBoats()
    {
        boats++;
        if (collectSound != null)
        {
            AudioSource.PlayClipAtPoint(collectSound, transform.position);
        }
        BoatText = GameObject.Find("BotText").GetComponent<Text>();
        BoatText.text = "" + boats;
        changeHint("You need to find gloves! Don't gorget to collect diamonds!!");

        GameObject.Find("FPSController").GetComponent<FirstPersonController>().m_WalkSpeed =8;
        GameObject.Find("FPSController").GetComponent<FirstPersonController>().m_RunSpeed =12;
    }

    public void onGetDiamond()
    {
        diamonds++;
        if (collectSound != null)
        {
            AudioSource.PlayClipAtPoint(collectSound, transform.position);
        }
        if (diamonds >= 4)
        {
            weatherController = GameObject.Find("FX").GetComponent<FXDemoController>();
            weatherController.Storm();
            changeHint("You need to move faster! It's getting colder!!!");
        }
        if (diamonds >= 7)
        {
            Destroy(GameObject.Find("NotAllow"));
            Destroy(GameObject.Find("NotAllow2"));
            doorLight = GameObject.Find("doorLight").GetComponent<Light>();
            doorLight.color = Color.green;
            KapiAcilsinMi = true;
            
        }
        DiamondText = GameObject.Find("DiamondText").GetComponent<Text>();
        DiamondText.text = "" + diamonds;
    }

    public void onGetLifepot()
    {
        health+=20;
        if (collectSound != null)
        {
            AudioSource.PlayClipAtPoint(collectSound, transform.position);
        }
        
    }
    public void changeHint(string newHint)
    {
        HintText = GameObject.Find("HintText").GetComponent<Text>();
        HintText.text = newHint;
    }

    public void healthDecrease(string name)
    {

        int rand = Random.Range(5, 20);
        if (name == "Golem")
        {
            rand = Random.Range(30, 60);
        }
        
        health -= rand;
        healthBar.value = health;
        //Debug.Log("HEALTH: " + health);
       
        
    }
    public void KapidanGecti()
    {
        if (woods < 10)
        {
            changeHint("You need to find 10 woods for fire before dying because of cold weather!!!!");
        }
        if (cikisSayisi == 0)
        {
            doorLight.color = Color.red;
            cikisSayisi++;
            KapiAcilsinMi = false;
            //weatherController = GameObject.Find("FX").GetComponent<FXDemoController>();
            //weatherController.Storm();

        }
    }
}
