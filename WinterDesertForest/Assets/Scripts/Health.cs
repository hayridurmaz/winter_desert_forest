﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Collections;

public class Health : MonoBehaviour
{
    private int minHealth = 1;
    static int maxHealth = 300;
    Animator enemyAnim;
    

    private void OnCollisionEnter(Collision collision)
    {
        
        
        if (collision.gameObject.tag == "Enemy")
        {
            maxHealth = maxHealth - Random.Range(10, 50);
            print(maxHealth);
            if (maxHealth < minHealth )
            {
                enemyAnim.SetBool("isAttacking", false);
                enemyAnim.SetBool("isDead", true);
                Destroy(collision.gameObject,3);
            }
            else
            {
                enemyAnim.SetBool("isAttacking", true);
                enemyAnim.SetBool("isDead", false);
            }
           
        }
        }
    private void Start()
    {
        
        enemyAnim = GetComponent<Animator>();
    }
    void Update()
    {


    }

}
