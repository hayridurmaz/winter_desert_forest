﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot : MonoBehaviour {
    public Rigidbody ShootArrow;
    public float power;
    public float moveSpeed;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
        float h = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
        float v = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;

        if (Input.GetButtonUp("Fire1"))
        {
            Rigidbody instance = Instantiate(ShootArrow, transform.position,
transform.rotation) as Rigidbody;

            Vector3 fwd = transform.TransformDirection(Vector3.forward);
            instance.AddForce(fwd * power);
            Destroy(instance.gameObject, 1);

        }

    }
}
