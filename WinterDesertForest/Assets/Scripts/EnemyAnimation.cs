﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour {
    public Animator enemyAnim;

	// Use this for initialization
	void Start () {
        enemyAnim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown("1")){
            enemyAnim.Play("Get_hit", -1, 0.0f);
        }
	}
}
