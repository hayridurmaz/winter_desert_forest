﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour {

public void PlayGame()
    {
        LoadingManager.willBeLoaded = "Winter";
        SceneManager.LoadScene("Loading");

    }
    public void QuitGame()
    {
        Debug.Log("QUIT!!");
        Application.Quit();

    }

    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    void Update()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

    }
}
