﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fire2 : MonoBehaviour
{
    Renderer rend, rend2, rend3, rend4, rend5;
    BoxCollider box, box2, box3, box4, box5;
    public Slider healthBar;
    // Use this for initialization
    void Start()
    {
        rend = GameObject.FindGameObjectWithTag("Fire1").GetComponent<Renderer>();
        box = GameObject.FindGameObjectWithTag("Fire1").GetComponent<BoxCollider>();
        box.enabled = false;
        rend.enabled = false;
        rend2 = GameObject.FindGameObjectWithTag("Fire2").GetComponent<Renderer>();
        rend2.enabled = false;
        rend3 = GameObject.FindGameObjectWithTag("Fire3").GetComponent<Renderer>();
        rend3.enabled = false;
        rend4 = GameObject.FindGameObjectWithTag("Fire4").GetComponent<Renderer>();
        rend4.enabled = false;
        rend5 = GameObject.FindGameObjectWithTag("Fire5").GetComponent<Renderer>();
        box5 = GameObject.FindGameObjectWithTag("Fire5").GetComponent<BoxCollider>();
        box5.enabled = true;
        rend5.enabled = true;
    }

  
    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Fireee");
            rend.enabled = true;
            box.enabled = true;
            rend2.enabled = true;
            rend3.enabled = true;
            rend4.enabled = true;
            
            if (healthBar.value < 1) {
                box5.enabled = false;
                rend5.enabled = false;
            }
            

        }


    }
}
