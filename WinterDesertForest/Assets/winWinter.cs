﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class winWinter : MonoBehaviour {


    float timer;
    bool timerActive = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (timerActive)
        {
            timer += Time.deltaTime;
        }


        if (timer > 3)
        {
            LoadingManager.willBeLoaded = "Forest";
            SceneManager.LoadScene("Loading");
        }
	}

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            //SceneManager.LoadScene("Desert");

            if (WinterSceneManager.diamonds >= 7)
            {
                other.GetComponent<WinterSceneManager>().changeHint("GREAT JOB!");
                timerActive = true;
                timer = 0;
                

            }
        }

    }
}
