﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class onTriggerItems : MonoBehaviour {
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (gameObject.tag == "Wood")
        {
            gameObject.transform.Rotate(new Vector3(0, 1, 0));

        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            //SceneManager.LoadScene("Desert");

            WinterSceneManager sn = gameObject.GetComponent<WinterSceneManager>();
            if (gameObject.tag == "Wood")
            {
                sn.onGetWood();
            }
            else if (gameObject.tag == "Glove")
            {
                sn.onGetGlove();
            }
            else if (gameObject.tag == "Boat")
            {
                sn.onGetBoats();
            }
            else if (gameObject.tag == "Diamond")
            {
                sn.onGetDiamond();
            }
            else if (gameObject.tag == "LifePot")
            {
                sn.onGetLifepot();
            }
            Destroy(gameObject);
        }

    }
}
