﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class createWeapon : MonoBehaviour {

    public Transform weapon;
    public Transform camera;
	// Use this for initialization
	void Start () {

        var a = Instantiate(weapon, camera, false);
        a.transform.Translate(new Vector3(0, 0, 1));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
