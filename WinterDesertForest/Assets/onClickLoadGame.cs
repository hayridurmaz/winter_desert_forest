﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class onClickLoadGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
        gameObject.GetComponent<Button>().onClick.AddListener(loadGame);
        string scene = (string)GlobalDataHolder.GetRouteParams("SCENE");
        if(scene=="Winter" || scene == "Desert" || scene == "Forest")
        {
            gameObject.GetComponent<Button>().onClick.AddListener(loadGame);
            gameObject.GetComponent<Button>().enabled = true;
        }
        else
        {
            gameObject.GetComponent<Button>().enabled = false;

        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void loadGame()
    {
        string scene =(string) GlobalDataHolder.GetRouteParams("SCENE");
        LoadingManager.willBeLoaded = scene;
        SceneManager.LoadScene("Loading");


    }

}
