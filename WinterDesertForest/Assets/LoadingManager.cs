﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour {

    public static string willBeLoaded="Menu";
	// Use this for initialization
	void Start () {
        SceneManager.LoadScene(willBeLoaded);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
