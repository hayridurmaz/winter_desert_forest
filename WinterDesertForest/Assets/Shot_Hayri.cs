﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shot_Hayri : MonoBehaviour {

    public float timer = 0.0f;
    public Rigidbody ShootArrow;
    public float power;
    public float moveSpeed;
    public float maxPower;
    public float powerUpSpeed = 300;
    public bool hold = false;
    public bool canAttack = false;

    // Use this for initialization
    void Start()
    {
        power = 1000;
        moveSpeed = 1000;
    }

    // Update is called once per frame
    void Update()
    {

        if (canAttack)
        {
            float h = Input.GetAxis("Horizontal") * Time.deltaTime * moveSpeed;
            float v = Input.GetAxis("Vertical") * Time.deltaTime * moveSpeed;

            if (Input.GetButtonUp("Fire1"))
            {
                Rigidbody instance = Instantiate(ShootArrow, transform.position,
    transform.rotation) as Rigidbody;

                Vector3 fwd = transform.TransformDirection(Vector3.forward);
                instance.AddForce(fwd * power);
                Destroy(instance.gameObject, 1);

            }
        }



    }
}
