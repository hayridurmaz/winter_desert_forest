﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class pause_manager_hayri : MonoBehaviour {
    bool pauseShown = false;

    // Use this for initialization
    void Start () {
        GameObject.Find("RestartBtn").GetComponent<Button>().onClick.AddListener(Restart);
        GameObject.Find("ResumeBtn").GetComponent<Button>().onClick.AddListener(Resume);
        GameObject.Find("QuitBtn").GetComponent<Button>().onClick.AddListener(Quit);
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            pauseShown = !pauseShown;
        }

        GameObject.Find("PauseCanvas").GetComponent<Canvas>().enabled = pauseShown;
        GameObject.Find("Canvas").GetComponent<Canvas>().enabled = !pauseShown;

        if (pauseShown)
        {
            Time.timeScale = 0;
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            GameObject.Find("Canvas").GetComponent<Canvas>().sortingOrder = 0;
            GameObject.Find("PauseCanvas").GetComponent<Canvas>().sortingOrder = 100;
        }
        else
        {
            Time.timeScale = 1;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            GameObject.Find("Canvas").GetComponent<Canvas>().sortingOrder = 100;
            GameObject.Find("PauseCanvas").GetComponent<Canvas>().sortingOrder = 0;

        }
    }

    void Restart()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        GlobalDataHolder.PutRouteParams("SCENE", SceneManager.GetActiveScene().name);
        MyNotifications.CallNotification("Game Saved!", 2);
        pauseShown = false;

    }


    void Resume()
    {
        Debug.Log("RESUME()");
        pauseShown = false;
        //string loaded =(string) GlobalDataHolder.GetRouteParams("SCENE");
        //Debug.Log(loaded);
    }

    void Quit()
    {
        SceneManager.LoadScene("Menu");
    }

}
